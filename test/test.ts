import { expect, tap } from '@pushrocks/tapbundle';
import * as sdk from '../ts/index.js';
import { TypedRouter } from '@apiglobal/typedrequest';

tap.test('should create a valid Handler', async () => {
  class MyHandler extends sdk.AAgHandler<any> {
    public slug: 'testapi';
    public typedrouter = new TypedRouter();

    public async checkReqirements() {
      return {
        allOk: true,
        reason: '',
      };
    }

    public async start() {}

    public async stop() {}

    public async checkRequirements(): Promise<sdk.IRequirementResult> {
      return {
        allOk: true,
        reason: '',
      };
    }
  }

  // tslint:disable-next-line: max-classes-per-file
  class AgEnvironement extends sdk.AgEnvironment {
    public async getEnvVar(nameArg: string) {
      return '';
    }
  }

  const myHandlerInstance = new MyHandler(new AgEnvironement());

  expect(myHandlerInstance).toBeInstanceOf(sdk.AAgHandler);
});

tap.start();
