/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@apiglobal/sdk',
  version: '2.0.1',
  description: 'an sdk package for api.global'
}
