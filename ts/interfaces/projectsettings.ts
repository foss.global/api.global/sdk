export interface IHandlerSettings {
  /**
   * the slug is a globally unique slug that the package will be answer under
   */
  slug: string;
  /**
   * the packagename points to the package to use in the supplied registry
   */
  packageName: string;
  /**
   * environment variables are supplied to the service at runtime
   */
  environmentVariables: { [key: string]: string };
  /**
   * this is needed to know where to get the package to run
   */
  registryDescriptor: {
    registryUrl: string;
    registryToken: string;
  };
}
