import * as plugins from './sdk.plugins.js';

/**
 * AuthInfo is created for every single incoming request
 */
export abstract class AuthInfo<TClaim> {
  /**
   * wether the request is generally authenticated
   */
  public abstract authenticated: boolean;

  /**
   * A heuristic analysis result wether the request might be fishy.
   * Note: DDOS attacks should never make it anywhere near your code and are filtered by api.global itself.
   */
  public abstract potentiallyMalicious: boolean;

  public abstract claim: TClaim;
}
