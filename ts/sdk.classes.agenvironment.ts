import * as plugins from './sdk.plugins.js';

/**
 * AgEnvironment handles the provision of environment variables to handlers
 */
export abstract class AgEnvironment {
  public abstract getEnvVar(envVarName: string): Promise<string>;
}
