import * as plugins from './sdk.plugins.js';
import { AuthInfo } from './sdk.classes.authinfo.js';
import { AgEnvironment } from './sdk.classes.agenvironment.js';

export interface IRequirementResult {
  allOk: boolean;
  reason: string;
}

export abstract class AAgHandler<TClaim> {
  public agEnvironment: AgEnvironment;

  /**
   * a slug that separates the handler from other handlers
   */
  public abstract slug: string;
  public abstract typedrouter: plugins.typedrequest.TypedRouter;
  public abstract checkRequirements(): Promise<IRequirementResult>;

  public async checkQenvFile(pathToQenvFileArg: string) {
    if (!(await plugins.smartfile.fs.fileExists(pathToQenvFileArg))) {
      throw new Error(
        `AgHandler with slug '${this.slug}': qenv file does not exists at ${pathToQenvFileArg}`
      );
    }
    const qenvFile = plugins.smartfile.fs.toObjectSync(pathToQenvFileArg);
    const missingEnvironmentVariables: string[] = [];
    for (const envVar of qenvFile.required as string[]) {
      const result = this.agEnvironment.getEnvVar(envVar);
      if (!result) {
        missingEnvironmentVariables.push(envVar);
      }
    }
    if (missingEnvironmentVariables.length > 0) {
      console.log(
        `AgHandler with slug '${this.slug}': There are ${missingEnvironmentVariables.length} missing environment variables`
      );
      const errorMessage = `AgHandler with slug '${this.slug}': The missing env Varibles are ${missingEnvironmentVariables}`;
      console.log(errorMessage);
      throw new Error(errorMessage);
    }
  }

  constructor(agEnvironmentArg: AgEnvironment) {
    this.agEnvironment = agEnvironmentArg;
  }

  /**
   * start the ag-handler
   */
  public abstract start(): Promise<any>;
  public abstract stop(): Promise<any>;
}
